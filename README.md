# ics-ans-davfs2-cleanup

Ansible playbook to remove davfs2.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
