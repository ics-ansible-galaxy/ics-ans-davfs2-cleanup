import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('davfs2_cleanup')


def test_davfs2_is_uninstalled(host):
    davfs2 = host.package("davfs2")
    assert not davfs2.is_installed


def test_mount_is_unmounted(host):
    assert not host.mount_point("/home/operator/Nextcloud").exists
